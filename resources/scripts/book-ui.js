window.modalEvents = {};

window.registerModalEvent = function (modal_ident, cb) {
    modalEvents[modal_ident] = new CustomEvent(modal_ident);
    document.addEventListener(modal_ident, cb);
};

window.modalTrigger = function (modal_ident, cb = null) {
    document.dispatchEvent(modalEvents[modal_ident]);
    if (cb) cb();
};

window.confirmDelete = function (event, form_id) {
    event.preventDefault();
    let popup = confirm('Are you sure you want to delete this?');
    if (!popup) {
        return;
    }
    const form = document.getElementById(form_id);
    form.submit();
};
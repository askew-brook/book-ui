<div class="{{ $classes('container') }}">
    <div class="{{ $classes('border') }}">
        <table {{ $attributes->merge(['class' => $classes('master')]) }}>
            <thead class="{{ $classes('header') }}">
            <tr>
                {{ $head }}
            </tr>
            </thead>
            <tbody class="{{ $classes('body') }}">
            {{ $body ?? $slot }}
            </tbody>
        </table>
    </div>
</div>

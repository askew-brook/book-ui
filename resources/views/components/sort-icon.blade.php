@props(['field', 'sortField', 'sortAsc', 'class' => 'inline-block w-5 h-5 fill-current'])
@if ($sortField !== $field)
    <x-fas-sort class="{{ $class }}" />
@elseif ($sortAsc)
    <x-fas-sort-up class="{{ $class }}" />
@else
    <x-fas-sort-down class="{{ $class }}" />
@endif
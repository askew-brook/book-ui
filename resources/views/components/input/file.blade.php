<label class="{{ $classes('group') }}">
    @if($label??'' !== false)
        <x-book-input.label :note="$note"
                            :tooltip="$tooltip">{{ $label ?? Str::snakeToTitle($name) }}</x-book-input.label>
    @endif
    <div class="{{ $classes('container') }}">
        @if($prepend) <span class="{{ $classes('prepend') }}">{{ $prepend }}</span> @endif
        <input class="{{ $classes('master') }}"
               {{ $attributes }}
               name="{{ $internal ?? $name }}"
               type="file"
               value="{{ $value ?? old($internal ?? $name) }}">
        @if($append) <span class="{{ $classes('append') }}">{{ $append }}</span> @endif
    </div>
    <x-book-input.error :name="$internal ?? $name"/>
</label>

<div class="{{ $classes('group') }}">
    <div {{ $attributes->merge(['class' => $classes('master')]) }} title="{{ $title ?? '' }}"
         x-data="{ on: {{ $value ?? (int) old($internal ?? $name, 0) }} }">
        <span class="{{ $classes('label') }}"
              @if($title ?? false) style="cursor: help" @endif
              @click="on = !on">{{ $label ?? Str::snakeToTitle($name) }}</span>
        <span role="checkbox"
              tabindex="0"
              @click="on = !on"
              @keydown.space.prevent="on = !on"
              :aria-checked="on.toString()"
              aria-checked="false"
              :class="{ '{{ $classes('switcher.socket.off') }}': !on, '{{ $classes('switcher.socket.on') }}': on }"
              class="{{ $classes('switcher.socket.main') }}">
            <span aria-hidden="true"
                  :class="{ 'translate-x-5': on, 'translate-x-0': !on }"
                  class="{{ $classes('switcher.dial.main') }}">
                <span :class="{ '{{ $classes('switcher.dial.false-icon.on') }}': on, '{{ $classes('switcher.dial.false-icon.off') }}': !on }"
                      class="{{ $classes('switcher.dial.false-icon.main') }}">
                    <svg class="{{ $classes('switcher.dial.false-icon.svg') }}" fill="none" viewBox="0 0 12 12">
                        <path d="M4 8l2-2m0 0l2-2M6 6L4 4m2 2l2 2"
                              stroke="currentColor"
                              stroke-width="2"
                              stroke-linecap="round"
                              stroke-linejoin="round"></path>
                    </svg>
                </span>
                <span :class="{ '{{ $classes('switcher.dial.true-icon.on') }}': on, '{{ $classes('switcher.dial.true-icon.off') }}': !on }"
                      class="{{ $classes('switcher.dial.true-icon.main') }}">
                    <svg class="{{ $classes('switcher.dial.true-icon.svg') }}" fill="currentColor" viewBox="0 0 12 12">
                        <path d="M3.707 5.293a1 1 0 00-1.414 1.414l1.414-1.414zM5 8l-.707.707a1 1 0 001.414 0L5 8zm4.707-3.293a1 1 0 00-1.414-1.414l1.414 1.414zm-7.414 2l2 2 1.414-1.414-2-2-1.414 1.414zm3.414 2l4-4-1.414-1.414-4 4 1.414 1.414z"></path>
                    </svg>
                </span>
            </span>
        </span>
        <input type="hidden" name="{{ $internal??$name }}" :value="+ on">
    </div>
</div>

<label class="{{ $classes('group') }}">
    @if($label??'' !== false)
        <x-book-input.label :note="$note"
                            :tooltip="$tooltip">{{ $label ?? Str::snakeToTitle($name) }}</x-book-input.label>
    @endif
    <div class="{{ $classes('container') }}">
        <textarea name="{{ $internal ?? $name }}"
                  class="{{ $classes('master') }}" {{ $attributes }}>{{ $slot ?? $value ?? old($internal ?? $name) }}</textarea>
    </div>
    <x-book-input.error :name="$internal ?? $name"/>
</label>

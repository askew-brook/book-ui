<span class="{{ $classes('master') }}">
    {{ $slot }}
    @if($tooltip)
        <span style="cursor: help" title="{{ $tooltip }}" class="ml-2">
            <x-fas-question-circle class="{{ $classes('tooltip') }}"/>
        </span>
    @endif
    @if($note)<span class="{{ $classes('note') }}">{{ $note }}</span> @endif
    {{ $extra }}
</span>
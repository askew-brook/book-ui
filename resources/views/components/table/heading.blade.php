<th {{ $attributes->merge(['class' => $classes('master')]) }}>
    @if($sortable)
        <a wire:click="sortBy('{{ $label ?? $slot }}')" class="{{ $classes('sorting') }}">
            @if($slot->isEmpty()){{ $label }}@else{{ $slot }}@endif
            <x-book-sort-icon :sortField="$sortField" :sortAsc="$sortAsc" :field="$label"/>
        </a>
    @else
        {{ $slot }}
    @endif
</th>
@unless($hiddenButton)
@if($customButton)
    <a href=""
       x-data=""
       class="{{ $buttonClass }}"
       @click.prevent="modalTrigger('{{ $randomIdent() }}', () => { {{ $callback }} })">{{ $label }}</a>
@else
    <x-book-button x-data=""
                   :class="$buttonClass"
                   @click.prevent="modalTrigger('{{ $randomIdent() }}', () => { {{ $callback }} })">{{ $label }}</x-book-button>
@endif
@endif

@unless($inline) @push('modals') @endunless
<div x-data="{ open: {{ $open ? 'true' : 'false' }} }"
     x-init="registerModalEvent('{{ $randomIdent() }}', e => {
        $dispatch('modal-close');
        open = !open;
     });"
     x-on:keydown.escape.window="open = false"
     x-on:modal-close.window="open = false"
     wire:ignore.self
     x-cloak
     class="inline">
    <div x-cloak
         x-show="open"
         wire:ignore.self
         class="{{ $classes('outer') }}">

        <div x-show="open"
             wire:ignore.self
             @if($offclick) @click="open = false" @endif
             x-description="Background overlay, show/hide based on modal state."
             x-transition:enter="ease-out duration-300"
             x-transition:enter-start="opacity-0"
             x-transition:enter-end="opacity-100"
             x-transition:leave="ease-in duration-200"
             x-transition:leave-start="opacity-100"
             x-transition:leave-end="opacity-0"
             class="{{ $classes('shadow.outer') }}">
            <div class="{{ $classes('shadow.inner') }}"></div>
        </div>

        <div x-show="open"
             wire:ignore.self
             x-description="Modal panel, show/hide based on modal state."
             x-transition:enter="ease-out duration-300"
             x-transition:enter-start="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
             x-transition:enter-end="opacity-100 translate-y-0 sm:scale-100"
             x-transition:leave="ease-in duration-200"
             x-transition:leave-start="opacity-100 translate-y-0 sm:scale-100"
             x-transition:leave-end="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
             class="{{ $classes('modal') }}"
             role="dialog"
             aria-modal="true"
             aria-labelledby="modal-headline">
            {{ $slot }}
        </div>
    </div>
</div>
@unless($inline) @endpush @endunless


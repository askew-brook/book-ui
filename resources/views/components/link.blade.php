<{{ $component }} {{ $attributes->merge(['class' => $classes()]) }}>
{{ $slot }}
</{{ $component  }}>
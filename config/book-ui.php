<?php

return [
    'prefix'     => 'book',
    'components' => [
        'button'         => \BookUI\Components\Button::class,
        'button.delete'  => \BookUI\Components\Button\Delete::class,
        'button.outline' => \BookUI\Components\Button\Outline::class,
        'link'           => \BookUI\Components\Link::class,

        'modal' => \BookUI\Components\Modal::class,

        'table'         => \BookUI\Components\Table::class,
        'table.heading' => \BookUI\Components\Table\Heading::class,
        'table.data'    => \BookUI\Components\Table\Data::class,

        'input.error'    => \BookUI\Components\Input\Error::class,
        'input.file'     => \BookUI\Components\Input\File::class,
        'input.label'    => \BookUI\Components\Input\Label::class,
        'input.select'   => \BookUI\Components\Input\Select::class,
        'input.text'     => \BookUI\Components\Input\Text::class,
        'input.textarea' => \BookUI\Components\Input\Textarea::class,
        'input.toggle'   => \BookUI\Components\Input\Toggle::class,
    ],

    // these are components that should be loaded without a paired class
    // these are normally super basic
    'anonymous'  => [
        'sort-icon'
    ],

    // here you can define new themes or modify existing ones,
    // these are consumed by the components as class lists,
    // conditionals are only stored on the components.
    // the key names match the components above by default but
    // can be overridden by providing the theme="---" parameter on any component
    'themes'     => [
        'button'         => [
            'master'   => [
                'inline-flex',
                'justify-center',
                'focus:outline-none',
                "focus:shadow-outline-blue",
                "active:bg-blue-700",
                'transition',
                'duration-150',
                'ease-in-out'
            ],
            'ColourBG' => ["bg-blue-500", "hover:bg-blue-400", 'text-white'],
            'border'   => ['border-2', 'border-blue-500', 'hover:border-blue-400', "focus:border-blue-700", 'rounded'],
            'rounding' => ['rounded-none'],
            'paddingX' => ['px-4'],
            'paddingY' => ['py-2'],
            'text'     => ['text-sm'],
        ],
        // inherits from button
        'button-outline' => [
            'ColourBG' => ['bg-transparent', "hover:bg-blue-500"],
            'border'   => ['border-2', "border-blue-500", 'rounded'],
            'text'     => ['text-sm', "text-blue-500", 'hover:text-white'],
            'font'     => ['font-semibold']
        ],

        'input'          => [
            'group'     => ['block', 'mb-5'],
            'container' => [
                'base' => ['mt-1', 'rounded-sm', 'shadow-sm'],
                'pend' => ['flex'],
            ],
            'master'    => [
                'input'   => [
                    'form-input',
                    'sm:text-sm',
                    'sm:leading-5',
                    'block',
                    'w-full',
                    'shadow-sm',
                    'appearance-none',
                    'focus:outline-none',
                    'placeholder-gray-400',
                    "focus:shadow-outline-blue",
                    'transition',
                    'duration-150',
                    'ease-in-out',
                    "focus:border-blue-300"
                ],
                'prepend' => ['border-l-0 rounded-l-none'],
                'append'  => ['border-r-0 rounded-r-none'],
                'error'   => ['border-red-300 text-red-900 placeholder-red-300 focus:border-red-300 focus:shadow-outline-red'],
            ],
            'prepend'   => [
                'flex',
                'items-center',
                'justify-center',
                'p-1',
                'px-2',
                'text-sm',
                'text-gray-500',
                'bg-gray-100',
                'border-r-0',
                'rounded-r-none',
                'shadow-sm',
                'form-input'
            ],
            'append'    => [
                'flex',
                'items-center',
                'whitespace-no-wrap',
                'justify-center',
                'px-2',
                'text-sm',
                'text-gray-500',
                'bg-gray-100',
                'border-l-0',
                'rounded-l-none',
                'shadow-sm',
                'form-input'
            ],
        ],
        'input-error'    => [],
        'input-label'    => [
            'master'  => [
                'display' => ['flex', 'items-center'],
                'text'    => ['text-sm', 'font-medium', 'leading-5']
            ],
            'note'    => ['text-xs', 'ml-1', 'self-end', 'font-medium', "text-blue-600"],
            'tooltip' => ['inline', 'w-4', 'pb-1', "text-blue-600", 'fill-current'],
        ],
        // inherits from input
        'input-select'   => [],
        // inherits from input
        'input-text'     => [],
        // inherits from input
        'input-file'     => [],
        // inherits from input
        'input-textarea' => [],
        // inherits from input
        'input-toggle'   => [
            'master'   => ['flex', 'items-center', 'self-end', 'space-x-2', 'md:pb-2'],
            'label'    => ['text-sm', 'font-medium', 'leading-5'],
            'switcher' => [
                'socket' => [
                    'main' => [
                        'relative',
                        'inline-flex',
                        'flex-shrink-0',
                        'h-6',
                        'transition-colors',
                        'duration-200',
                        'ease-in-out',
                        'bg-gray-200',
                        'border-2',
                        'border-transparent',
                        'rounded-full',
                        'cursor-pointer',
                        'w-11',
                        'focus:outline-none',
                        "focus:shadow-outline-blue"
                    ],
                    'on'   => ["bg-blue-600"],
                    'off'  => ['bg-gray-200'],
                ],
                'dial'   => [
                    'main'       => [
                        'relative',
                        'inline-block',
                        'w-5',
                        'h-5',
                        'transition',
                        'duration-200',
                        'ease-in-out',
                        'transform',
                        'translate-x-0',
                        'bg-white',
                        'rounded-full',
                        'shadow'
                    ],
                    'false-icon' => [
                        'main' => [
                            'absolute',
                            'inset-0',
                            'flex',
                            'items-center',
                            'justify-center',
                            'w-full',
                            'h-full',
                            'transition-opacity',
                            'duration-200',
                            'ease-in',
                            'opacity-100'
                        ],
                        'on'   => ['opacity-0', 'ease-out', 'duration-100'],
                        'off'  => ['opacity-100', 'ease-in', 'duration-200'],
                        'svg'  => ['w-3', 'h-3', 'text-gray-400'],
                    ],
                    'true-icon'  => [
                        'main' => [
                            'absolute',
                            'inset-0',
                            'flex',
                            'items-center',
                            'justify-center',
                            'w-full',
                            'h-full',
                            'transition-opacity',
                            'duration-100',
                            'ease-out',
                            'opacity-0'
                        ],
                        'on'   => ['opacity-100', 'ease-in', 'duration-200'],
                        'off'  => ['opacity-0', 'ease-out', 'duration-100'],
                        'svg'  => ['w-3', 'h-3', "text-blue-600"],
                    ],
                ],
            ],
        ],

        'table'         => [
            'container' => ['py-2 -my-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8'],
            'border'    => ['align-middle inline-block min-w-full shadow overflow-hidden border-b border-gray-200'],
            'master'    => ['min-w-full'],
            'header'    => [],
            'body'      => [],
        ],
        'table-heading' => [
            'master'  => [
                'px-6',
                'py-3',
                'border-b',
                'border-gray-200',
                'bg-gray-50',
                'text-left',
                'text-xs',
                'leading-4',
                'font-medium',
                'text-gray-500',
                'uppercase',
                'tracking-wider'
            ],
            'sorting' => [
                'cursor-pointer',
                'w-full'
            ],
        ],
        'table-data'    => [
            'master'     => ['px-6 py-4 text-sm leading-5 font-medium text-gray-900'],
            'whitespace' => ['whitespace-no-wrap'],
        ],

        'modal' => [
            'outer'  => [
                'fixed',
                'inset-x-0',
                'bottom-0',
                'px-4',
                'pb-6',
                'sm:inset-0',
                'sm:p-0',
                'sm:px-2',
                'sm:flex',
                'sm:items-center',
                'sm:justify-center',
                'z-10'
            ],
            'shadow' => [
                'outer' => ['fixed', 'inset-0', 'transition-opacity'],
                'inner' => ['absolute', 'inset-0', 'bg-gray-500', 'opacity-75'],
            ],
            'modal'  => [
                'main'   => [
                    'bg-white',
                    'rounded-lg',
                    'px-4',
                    'pt-5',
                    'pb-4',
                    'shadow-xl',
                    'transform',
                    'transition-all',
                    'sm:w-full',
                    'sm:p-6'
                ],
                'small'  => ['sm:max-w-lg'],
                'medium' => ['sm:max-w-2xl'],
                'large'  => ['sm:max-w-4xl'],
            ]
        ],
    ]
];
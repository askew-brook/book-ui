<?php

namespace BookUI;

use BookUI\Components\Component;
use BookUI\Components\LivewireComponent;
use Illuminate\Contracts\Foundation\CachesConfiguration;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;
use Illuminate\View\Compilers\BladeCompiler;
use Livewire\Livewire;

final class BookUIServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/book-ui.php', 'book-ui');

        $this->commands([
            Console\InstallCommand::class,
            Console\PublishCommand::class,
            Console\PublishComponentCommand::class
        ]);
    }

    /**
     * Merge the given configuration with the existing configuration.
     *
     * @param string $path
     * @param string $key
     * @return void
     */
    protected function mergeConfigFrom($path, $key)
    {
        if (!($this->app instanceof CachesConfiguration && $this->app->configurationIsCached())) {
            $config = $this->app['config']->get($key, []);
            $vendor_config = require $path;
            $config['themes'] = array_merge($vendor_config['themes'], $config['themes']);
            $this->app['config']->set($key, array_merge($vendor_config, $config));
        }
    }

    public function boot(): void
    {
        $this->bootResources();
        $this->bootBladeComponents();
        $this->bootLivewireComponents();
        $this->bootDirectives();
        $this->bootPublishing();

        Str::macro('snakeToTitle', function ($value) {
            return \Str::of(str_replace('_', ' ', $value))->title();
        });
    }

    private function bootResources(): void
    {
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'book-ui');
    }

    private function bootBladeComponents(): void
    {
        $this->callAfterResolving(BladeCompiler::class, function (BladeCompiler $blade) {
            $prefix = config('book-ui.prefix', '');

            /** @var Component $component */
            foreach (config('book-ui.components', []) as $alias => $component) {
                $blade->component($component, $alias, $prefix);
            }

            // now we load the anonymous components that do not utilise a class
            foreach (config('book-ui.anonymous', []) as $name) {
                $blade->component('book-ui::components.' . $name, $name, $prefix);
            }
        });
    }

    private function bootLivewireComponents(): void
    {
        // Skip if Livewire isn't installed.
        if (!class_exists(Livewire::class)) {
            return;
        }
        $prefix = config('blade-ui-kit.prefix', '');

        /** @var LivewireComponent $component */
        foreach (config('blade-ui-kit.livewire', []) as $alias => $component) {
            $alias = $prefix ? "$prefix.$alias" : $alias;

            Livewire::component($alias, $component);
        }
    }

    private function bootDirectives(): void
    {
        Blade::directive('bookScripts', function () {
            if (file_exists(public_path('vendor/book-ui/book-ui.js'))) {
                return "<?php echo '<script src=\"'.asset('vendor/book-ui/book-ui.js').'\" /></script>' ?>";
            }
            $raw_js = file_get_contents(__DIR__ . '/../resources/scripts/book-ui.js');
            return "<script type='application/javascript'>{$raw_js}</script>";
        });
    }

    private function bootPublishing(): void
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../config/book-ui.php' => $this->app->configPath('book-ui.php'),
            ], 'book-ui-config');

            $this->publishes([
                __DIR__ . '/../resources/views' => $this->app->resourcePath('views/vendor/book-ui'),
            ], 'book-ui-views');

            $this->publishes([
                __DIR__ . '/../resources/scripts' => public_path('/vendor/book-ui')
            ], 'book-ui-scripts');
        }
    }
}
<?php

namespace BookUI\Console;

use Illuminate\Console\Command;

class InstallCommand extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'book-ui:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install all of the BookUI resources';

    public function handle()
    {
        $this->comment('Publishing BookUI Assets / Config...');
        $this->call('book-ui:publish');

        $this->info('BookUI installed successfully.');
    }
}
<?php

namespace BookUI\Console;

use Illuminate\Console\Command;

class PublishCommand extends Command
{
    protected $signature = 'book-ui:publish {--force : Overwrite and existing files}';

    protected $description = 'Publish all of the BookUI resources';

    public function handle()
    {
        $this->call('vendor:publish', [
            '--tag'   => 'book-ui-config',
            '--force' => $this->option('force'),
        ]);

        $this->call('vendor:publish', [
            '--tag'   => 'book-ui-scripts',
            '--force' => true,
        ]);

        $this->call('view:clear');
    }
}

<?php

namespace BookUI\Components;

use BookUI\Traits\ManagesClasses;

abstract class Component extends \Illuminate\View\Component
{
    use ManagesClasses;

    public function __construct($theme = null)
    {
        if ($theme) {
            $this->setTheme($theme);
        }
    }

    public function fill($values): void
    {
        $publicProperties = array_keys($this->getPublicPropertiesDefinedBySubClass());

        foreach ($values as $key => $value) {
            if (in_array($key, $publicProperties)) {
                $this->{$key} = $value;
            }
        }
    }

    public function getPublicPropertiesDefinedBySubClass(): array
    {
        $publicProperties = (new \ReflectionClass($this))->getProperties(\ReflectionProperty::IS_PUBLIC);
        $data = [];

        foreach ($publicProperties as $property) {
            if ($property->getDeclaringClass()->getName() !== self::class) {
                $data[$property->getName()] = $this->getInitializedPropertyValue($property);
            }
        }

        return $data;
    }

    public function getInitializedPropertyValue(\ReflectionProperty $property)
    {
        // Ensures typed property is initialized in PHP >=7.4, if so, return its value,
        // if not initialized, return null (as expected in earlier PHP Versions)
        if (method_exists($property, 'isInitialized') && !$property->isInitialized($this)) {
            return null;
        }

        return $property->getValue($this);
    }
}
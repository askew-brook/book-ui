<?php

namespace BookUI\Components;

use Illuminate\Database\Eloquent\Model;

abstract class Input extends Component
{
    protected static string $theme = 'input';
    public ?string $prepend;
    public ?string $append;
    public string $name;
    public ?Model $bind;
    public ?string $value;
    public ?string $note;
    public ?string $internal;
    public ?string $label;
    public ?string $tooltip;

    public function __construct($name, $prepend = null, $append = null, $bind = null, $value = null, $note = null, $internal = null, $label = null, $tooltip = null, $theme = null)
    {
        parent::__construct($theme);
        $this->fill([
            'name'     => $name,
            'prepend'  => $prepend,
            'append'   => $append,
            'bind'     => $bind,
            'value'    => $value,
            'note'     => $note,
            'internal' => $internal,
            'label'    => $label,
            'tooltip'  => $tooltip,
        ]);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('book-ui::components.input.base');
    }

    protected function conditionalList(): array
    {
        return [
            'prepend'        => $this->isPrepend(),
            'append'         => $this->isAppend(),
            'container.pend' => $this->isPrepend() || $this->isAppend(),
            'master.prepend' => $this->isPrepend(),
            'master.append'  => $this->isAppend(),
            'master.error'   => false,
        ];
    }

    protected function isPrepend()
    {
        return $this->prepend;
    }

    protected function isAppend()
    {
        return $this->append;
    }
}
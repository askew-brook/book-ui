<?php

namespace BookUI\Components;


class Modal extends Component
{
    protected static string $theme = 'modal';
    public ?string $label = null;
    public ?string $identifier = null;
    public bool $customButton = false;
    public ?string $buttonClass = null;
    public ?string $callback = null;
    public bool $inline = false;
    public bool $open = false;
    public bool $offclick = true;
    public string $size = 'small';
    public bool $hiddenButton = false;

    public function __construct($label = null, $identifier = null, $customButton = false, $buttonClass = null, $callback = null, $inline = false, $open = false, $offclick = true, $size = 'small', $hiddenButton = false, $theme = null)
    {
        parent::__construct($theme);
        $this->fill([
            'label'        => $label,
            'identifier'   => $identifier,
            'buttonClass'  => $buttonClass,
            'callback'     => $callback,
            'customButton' => $customButton,
            'inline'       => $inline,
            'open'         => $open,
            'offclick'     => $offclick,
            'size'         => $size,
            'hiddenButton' => $hiddenButton,
        ]);
    }

    public function randomIdent()
    {
        return md5($this->label . $this->identifier);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('book-ui::components.modal');
    }

    protected function conditionalList(): array
    {
        return [
            'modal.small'  => $this->size === 'small',
            'modal.medium' => $this->size === 'medium',
            'modal.large'  => $this->size === 'large'
        ];
    }
}
<?php

namespace BookUI\Components;

class Table extends Component
{
    protected static string $theme = 'table';

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('book-ui::components.table');
    }
}
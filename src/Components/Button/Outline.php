<?php

namespace BookUI\Components\Button;

use BookUI\Components\Button;

class Outline extends Button
{
    protected static string $theme = 'button-outline';

    public function __construct($link = false, $theme = null)
    {
        parent::__construct($link, $theme);

        // merge classes with its parent
        $this->merge = $this->classList(parent::$theme);
    }
}
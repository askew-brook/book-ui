<?php

namespace BookUI\Components\Button;

use BookUI\Components\Button;

class Delete extends Button
{
    public ?string $action;
    public ?bool $clean;

    public function __construct($action, $link = false, $clean = true, $theme = null)
    {
        parent::__construct($link, $theme);
        $this->action = $action;
        $this->clean = $clean;
    }

    public function hash(): string
    {
        return md5($this->action);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('book-ui::components.button.delete');
    }

    protected function classList(): array
    {
        if ($this->clean) {
            return [];
        }
        return parent::classList();
    }
}
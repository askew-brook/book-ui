<?php

namespace BookUI\Components\Table;

use BookUI\Components\Component;

class Heading extends Component
{
    protected static string $theme = 'table-heading';
    public ?bool $sortable;
    public ?string $sortField;
    public ?string $sortAsc;
    public ?string $label;

    public function __construct($sortable = false, $sortField = null, $sortAsc = null, $label = null, $theme = null)
    {
        parent::__construct($theme);
        $this->fill([
            'sortable'  => $sortable,
            'sortField' => $sortField,
            'sortAsc'   => $sortAsc,
            'label'     => $label,
        ]);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('book-ui::components.table.heading');
    }
}

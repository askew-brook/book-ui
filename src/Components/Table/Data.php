<?php

namespace BookUI\Components\Table;

use BookUI\Components\Component;

class Data extends Component
{
    protected static string $theme = 'table-data';

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('book-ui::components.table.data');
    }

    protected function conditionalList(): array
    {
        return [
            'whitespace' => !$this->classPropertyContains(['whitespace-'])
        ];
    }
}
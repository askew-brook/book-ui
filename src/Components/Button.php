<?php

namespace BookUI\Components;

class Button extends Component
{
    protected static string $theme = 'button';
    public string $component = 'button';

    public function __construct($theme = null, $link = false)
    {
        parent::__construct($theme);
        $this->fill([
            // if link boolean added, then component should be an A tag
            'component' => ($link ? 'a' : $this->component),
        ]);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('book-ui::components.button.base');
    }

    protected function conditionalList(): array
    {
        return [
            'ColourBG' => !$this->classPropertyContains(['bg-']),
            'border'   => !$this->classPropertyContains(['border']),
            'rounding' => !$this->classPropertyContains(['rounded']),
            'paddingX' => !$this->classPropertyContains(['px-']),
            'paddingY' => !$this->classPropertyContains(['py-']),
            'text'     => !$this->classPropertyContains([
                'text-sm',
                'text-xs',
                'text-lg',
                'text-base',
                'text-xl',
                'text-2xl',
                'text-3xl'
            ])
        ];
    }
}

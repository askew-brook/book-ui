<?php

namespace BookUI\Components\Input;

use BookUI\Components\Input;

class Text extends Input
{
    protected static string $theme = 'input-text';

    public function __construct($name, $prepend = null, $append = null, $bind = null, $value = null, $note = null, $internal = null, $label = null, $tooltip = null, $theme = null)
    {
        parent::__construct($name, $prepend, $append, $bind, $value, $note, $internal, $label, $tooltip, $theme);
        $this->merge = $this->classList(parent::$theme);
    }
}
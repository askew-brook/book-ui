<?php

namespace BookUI\Components\Input;

use BookUI\Components\Component;

class Label extends Component
{
    protected static string $theme = 'input-label';
    public ?string $note = null;
    public ?string $tooltip = null;
    public ?string $extra = null;

    public function __construct($note = null, $tooltip = null, $extra = null, $theme = null)
    {
        parent::__construct($theme);
        $this->fill([
            'note'    => $note,
            'tooltip' => $tooltip,
            'extra'   => $extra
        ]);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('book-ui::components.input.label');
    }
}
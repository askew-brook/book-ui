<?php

namespace BookUI\Components\Input;

use BookUI\Components\Input;

class File extends Input
{
    protected static string $theme = 'input-file';

    public function __construct($name, $prepend = null, $append = null, $bind = null, $value = null, $note = null, $internal = null, $label = null, $tooltip = null, $theme = null)
    {
        parent::__construct($name, $prepend, $append, $bind, $value, $note, $internal, $label, $tooltip, $theme);
        $this->merge = $this->classList(parent::$theme);
        $this->withAttributes(['type' => 'file']);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('book-ui::components.input.file');
    }
}
<?php

namespace BookUI\Components\Input;

use BookUI\Components\Component;

class Error extends Component
{
    protected static string $theme = 'input-error';
    public string $name;

    public function __construct($name, $theme = null)
    {
        parent::__construct($theme);
        $this->name = $name;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('book-ui::components.input.error');
    }
}
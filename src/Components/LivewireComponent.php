<?php

namespace BookUI\Components;

use BookUI\Traits\ManagesClasses;

final class LivewireComponent extends \Livewire\Component
{
    use ManagesClasses;
}
<?php

namespace BookUI\Components;

class Link extends Component
{
    protected static string $theme = 'link';
    public string $component = 'a';
    public string $colour = 'blue';

    public function __construct($colour = null, $button = false)
    {
        $this->colour = $colour ?? $this->colour;
        $this->component = ($button ? 'button' : $this->component);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.book.link');
    }

    protected function classList(): array
    {
        return [
            'master' => ['ease-in-out', 'duration-300', 'transition-colors'],
            'text'   => ['text-gray-700', "hover:text-{$this->colour}-500", 'underline'],
            'size'   => ['text-sm sm:text-base'],
        ];
    }

    protected function conditionalList(): array
    {
        return [
            'size' => $this->classPropertyContains([
                'text-xs',
                'text-sm',
                'text-base',
                'text-lg',
                'text-xl',
                'text-2xl',
                'text-3xl',
                'text-4xl'
            ])
        ];
    }
}
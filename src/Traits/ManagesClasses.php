<?php

namespace BookUI\Traits;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use function PHPUnit\Framework\isNull;

trait ManagesClasses
{
    protected static string $theme;
    protected ?array $merge = null;

    public function classes($scope = null, $conditional = null): string
    {
        // if the conditional is a callable then check its not returning false
        if ($conditional && is_callable($conditional) && !$conditional()) {
            return '';
        }

        // if the conditional is false, dont allow it to pass
        if ($conditional === false) {
            return '';
        }

        // we also need to check if theres any conditionals set in the class via conditionalList
        // the conditional is ran here also
        if ($this->classListHasConditional($scope) && !$this->processClassListConditional($scope)) {
            return '';
        }

        if (!$scope && Arr::isAssoc($this->classList())) {
            $map = array_map(fn($inner) => $this->classes($inner), array_keys($this->classList()));
            return implode(' ', $map);
        }

        // if we have an assoc, loop through and call class list multiple times
        if ($scope && Arr::isAssoc(Arr::get($this->classList(), $scope, []))) {
            $map = array_map(fn($inner) => $this->classes($scope . '.' . $inner), array_keys(Arr::get($this->classList(), $scope)));
            return implode(' ', $map);
        }

        if ($scope) {
            return implode(' ', Arr::get($this->classList(), $scope, []));
        }

        return implode(' ', $this->classList());
    }

    protected function classListHasConditional($scope = null): bool
    {
        return !Arr::has($this->conditionalList(), $scope) ? false : true;
    }

    protected function conditionalList(): array
    {
        return [];
    }

    protected function processClassListConditional($scope = null)
    {
        $conditional = Arr::get($this->conditionalList(), $scope);

        // is it a closure?
        if (is_callable($conditional)) {
            return $conditional();
        }

        // its a boolean
        return $conditional;
    }

    protected function classList($override = null): array
    {
        if ($override ?? $this::$theme) {
            $theme = config("book-ui.themes." . ($override ?? $this::$theme));
            if ($theme === null) {
                throw new \RuntimeException("Theme provided returned null, please check: " . $override ?? $this::$theme . " in the config");
            }
            if ($this->merge) {
                return array_merge($this->merge, $theme);
            }
            return $theme;
        }
        return [];
    }

    protected function setTheme($theme): void
    {
        $this::$theme = $theme;
    }

    protected function classPropertyContains(?array $contains = null): bool
    {
        return Str::of($this->attributes->get('class'))->contains($contains);
    }
}